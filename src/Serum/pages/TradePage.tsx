import React, { useCallback, useEffect, useRef, useState } from "react";
import {
  Button,
  Col,
  Popover,
  Row,
  Select,
  Typography,
  Menu,
  Dropdown
} from "antd";
import styled from "styled-components";
import Orderbook from "../components/Orderbook";
import UserInfoTable from "../components/UserInfoTable";
import StandaloneBalancesDisplay from "../components/StandaloneBalancesDisplay";
import {
  getMarketInfos,
  getTradePageUrl,
  MarketProvider,
  useMarket,
  useMarketsList,
  useUnmigratedDeprecatedMarkets
} from "../utils/markets";
import TradeForm from "../components/TradeForm";
import TradesTable from "../components/TradesTable";
import LinkAddress from "../components/LinkAddress";
import DeprecatedMarketsInstructions from "../components/DeprecatedMarketsInstructions";
import FloatingElement from "../components/layout/FloatingElement";
import {
  DeleteOutlined,
  InfoCircleOutlined,
  PlusCircleOutlined
} from "@ant-design/icons";
import CustomMarketDialog from "../components/CustomMarketDialog";
import { notify } from "../utils/notifications";
import { useHistory, useParams } from "react-router-dom";
import { nanoid } from "nanoid";
import { DownOutlined, ArrowUpOutlined } from "@ant-design/icons";
import { TVChartContainer } from "../components/TradingView";
import logo from "../assets/logo.svg";
import FloatingElementBlock from "../components/layout/FloatingElementBlock";
import FloatingNavBar from "../components/layout/FloatingNavbar";
import { useTokenRegistry } from "../../explorer/providers/mints/token-registry";
import { TokenListProvider, TokenInfo } from "@solana/spl-token-registry";
const { Option, OptGroup } = Select;

const Wrapper = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  margin-top: -4em;
  padding: 16px 16px;
  .borderNone .ant-select-selector {
    border: none !important;
  }
`;

export default function TradePage() {
  // eslint-disable-next-line
  const { marketAddress } = useParams();
  //const marketAddress = "";
  useEffect(() => {
    if (marketAddress) {
      localStorage.setItem("marketAddress", JSON.stringify(marketAddress));
    }
  }, [marketAddress]);
  const history = useHistory();
  function setMarketAddress(address) {
    history.push(getTradePageUrl(address));
  }

  return (
    <MarketProvider
      marketAddress={marketAddress}
      setMarketAddress={setMarketAddress}
    >
      <TradePageInner />
    </MarketProvider>
  );
}

function TradePageInner() {
  const menu = (
    <Menu>
      <Menu.Item>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-start",
            alignItems: "flex-start",
            flexDirection: "row",
            width: "20%"
          }}
        >
          <img
            src={logo}
            style={{ height: "30px", width: "30px", paddingLeft: "0.2em" }}
          />
          <p style={{ marginTop: "10%", marginLeft: "10%" }}>MARKET ONE</p>
        </div>
      </Menu.Item>
      <Menu.Item>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-start",
            alignItems: "flex-start",
            flexDirection: "row",
            width: "20%"
          }}
        >
          <img src={logo} style={{ height: "30px", width: "30px" }} />
          <p style={{ marginTop: "5%", marginLeft: "2%" }}>MARKET ONE</p>
        </div>
      </Menu.Item>
      <Menu.Item>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-start",
            alignItems: "flex-start",
            flexDirection: "row",
            width: "20%"
          }}
        >
          <img src={logo} style={{ height: "30px", width: "30px" }} />
          <p style={{ marginTop: "5%", marginLeft: "2%" }}>MARKET ONE</p>
        </div>
      </Menu.Item>
    </Menu>
  );

  const {
    market,
    marketName,
    customMarkets,
    setCustomMarkets,
    setMarketAddress
  } = useMarket();
  const markets = useMarketsList();
  const [handleDeprecated, setHandleDeprecated] = useState(false);
  const [addMarketVisible, setAddMarketVisible] = useState(false);
  const deprecatedMarkets = useUnmigratedDeprecatedMarkets();
  const [dimensions, setDimensions] = useState({
    height: window.innerHeight,
    width: window.innerWidth
  });

  // useEffect(() => {
  //   document.title = marketName ? `${marketName} — Serum` : "Serum";
  // }, [marketName]);

  const changeOrderRef =
    useRef<({ size, price }: { size?: number; price?: number }) => void>();

  useEffect(() => {
    const handleResize = () => {
      setDimensions({
        height: window.innerHeight,
        width: window.innerWidth
      });
    };

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const width = dimensions?.width;
  const componentProps = {
    onChangeOrderRef: (ref) => (changeOrderRef.current = ref),
    onPrice: useCallback(
      (price) => changeOrderRef.current && changeOrderRef.current({ price }),
      []
    ),
    onSize: useCallback(
      (size) => changeOrderRef.current && changeOrderRef.current({ size }),
      []
    )
  };
  const component = (() => {
    console.log("Width", width);
    if (handleDeprecated) {
      return (
        <DeprecatedMarketsPage
          switchToLiveMarkets={() => setHandleDeprecated(false)}
        />
      );
    } else if (width < 1000) {
      return <RenderSmaller {...componentProps} screenWidth={width} />;
    } else if (width < 1450) {
      return <RenderSmall {...componentProps} screenWidth={width} />;
    } else {
      return <RenderNormal {...componentProps} screenWidth={width} />;
    }
  })();

  const onAddCustomMarket = (customMarket) => {
    const marketInfo = getMarketInfos(customMarkets).some(
      (m) => m.address.toBase58() === customMarket.address
    );
    if (marketInfo) {
      notify({
        message: `A market with the given ID already exists`,
        type: "error"
      });
      return;
    }
    const newCustomMarkets = [...customMarkets, customMarket];
    setCustomMarkets(newCustomMarkets);
    setMarketAddress(customMarket.address);
  };

  const onDeleteCustomMarket = (address) => {
    const newCustomMarkets = customMarkets.filter((m) => m.address !== address);
    setCustomMarkets(newCustomMarkets);
  };

  return (
    <>
      <CustomMarketDialog
        visible={addMarketVisible}
        onClose={() => setAddMarketVisible(false)}
        onAddCustomMarket={onAddCustomMarket}
      />
      <Wrapper>
        {/* <Row
          align="middle"
          style={{ paddingLeft: 5, paddingRight: 5 }}
          gutter={16}
        >
          <Col>
            <MarketSelector
              markets={markets}
              setHandleDeprecated={setHandleDeprecated}
              placeholder={'Select market'}
              customMarkets={customMarkets}
              onDeleteCustomMarket={onDeleteCustomMarket}
            />
          </Col>
          {market ? (
            <Col>
              <Popover
                content={<LinkAddress address={market.publicKey.toBase58()} />}
                placement="bottomRight"
                title="Market address"
                trigger="click"
              >
                <InfoCircleOutlined style={{ color: '#2abdd2' }} />
              </Popover>
            </Col>
          ) : null}
          <Col>
            <PlusCircleOutlined
              style={{ color: '#2abdd2' }}
              onClick={() => setAddMarketVisible(true)}
            />
          </Col>
          {deprecatedMarkets && deprecatedMarkets.length > 0 && (
            <React.Fragment>
              <Col>
                <Typography>
                  You have unsettled funds on old markets! Please go through
                  them to claim your funds.
                </Typography>
              </Col>
              <Col>
                <Button onClick={() => setHandleDeprecated(!handleDeprecated)}>
                  {handleDeprecated ? 'View new markets' : 'Handle old markets'}
                </Button>
              </Col>
            </React.Fragment>
          )}
        </Row> */}
        {width > 910 ? (
          <FloatingNavBar>
            <Row
              style={{
                height: "120px",
                backgroundColor: "#242731",
                borderRadius: 10,
                marginBottom: 30,
                flexWrap: "nowrap"
              }}
            >
              <Col
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  marginLeft: "2%",
                  borderRight: "solid 1px",
                  marginTop: 10,
                  marginBottom: 10,
                  paddingRight: 10,
                  borderColor: " rgba(228, 228, 228, 0.1)",
                  minWidth: "15%"
                }}
              >
                {/* <img src={logo} style={{ height: "3.5em" }} /> */}
                {/* <Dropdown
                  overlay={menu}
                  placement="bottomCenter"
                  arrow
                  overlayClassName="dropdown"
                >
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                      justifyItems: "flex-start",
                      cursor: "pointer",
                      flex: 2
                    }}
                  >
                    <div style={{ marginLeft: "10px", marginRight: "10px" }}>
                      <span style={{ fontWeight: "bold" }}>SRM/USDT</span>
                      <br />
                      <span style={{ color: "#808191", fontWeight: "bold" }}>
                        Serum
                      </span>
                    </div>{" "}
                    <DownOutlined
                      size={32}
                      style={{
                        fontSize: 20,
                        fontWeight: "bold",
                        marginBottom: "18px"
                      }}
                    />
                  </div>
                </Dropdown> */}
                <MarketSelector
                  markets={markets}
                  setHandleDeprecated={setHandleDeprecated}
                  placeholder={"Select market"}
                  customMarkets={customMarkets}
                  onDeleteCustomMarket={onDeleteCustomMarket}
                />
              </Col>
              <Col
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  minWidth: "15%",
                  borderRight: "solid 1px",
                  marginTop: 10,
                  marginBottom: 10,
                  paddingRight: 10,
                  borderColor: " rgba(228, 228, 228, 0.1)"
                }}
              >
                <span
                  style={{
                    fontStyle: "normal",
                    fontWeight: "bold",
                    fontSize: "12px",
                    lineHeight: "16px",
                    color: "#808191"
                  }}
                >
                  24h Change
                </span>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <ArrowUpOutlined
                    style={{
                      fontWeight: "bold",
                      fontSize: "14px",
                      color: "#4FBF67"
                    }}
                  />
                  <span
                    style={{
                      fontWeight: "bold",
                      fontSize: "14px",
                      color: "#4FBF67"
                    }}
                  >
                    +8.42%
                  </span>
                </div>
              </Col>
              <Col
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  justifyItems: "flex-start",
                  minWidth: "15%",
                  borderRight: "solid 1px",
                  marginTop: 10,
                  marginBottom: 10,
                  paddingRight: 10,
                  borderColor: " rgba(228, 228, 228, 0.1)"
                }}
              >
                <span
                  style={{
                    fontStyle: "normal",
                    fontWeight: "bold",
                    fontSize: "12px",
                    lineHeight: "16px",
                    color: "#808191"
                  }}
                >
                  Last Price
                </span>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "flex-start",
                    alignItems: "flex-start"
                  }}
                >
                  <span
                    style={{
                      fontWeight: "bold",
                      fontSize: "14px",
                      color: "#FFF"
                    }}
                  >
                    4.39 USDT
                  </span>
                </div>
              </Col>
              <Col
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  minWidth: "15%",
                  borderRight: "solid 1px",
                  marginTop: 10,
                  marginBottom: 10,
                  paddingRight: 10,
                  borderColor: " rgba(228, 228, 228, 0.1)"
                }}
              >
                <span
                  style={{
                    fontStyle: "normal",
                    fontWeight: "bold",
                    fontSize: "12px",
                    lineHeight: "16px",
                    color: "#808191"
                  }}
                >
                  24h Low
                </span>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <span
                    style={{
                      fontWeight: "bold",
                      fontSize: "14px",
                      color: "#FFF"
                    }}
                  >
                    4.15 USDT
                  </span>
                </div>
              </Col>
              <Col
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  minWidth: "15%",
                  borderRight: "solid 1px",
                  marginTop: 10,
                  marginBottom: 10,
                  paddingRight: 10,
                  borderColor: " rgba(228, 228, 228, 0.1)"
                }}
              >
                <span
                  style={{
                    fontStyle: "normal",
                    fontWeight: "bold",
                    fontSize: "12px",
                    lineHeight: "16px",
                    color: "#808191"
                  }}
                >
                  24h Heigh
                </span>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <span
                    style={{
                      fontWeight: "bold",
                      fontSize: "14px",
                      color: "#FFF"
                    }}
                  >
                    4.55 USDT
                  </span>
                </div>
              </Col>
              <Col
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  minWidth: "15%",
                  marginTop: 10,
                  marginBottom: 10,
                  paddingRight: 10
                }}
              >
                <span
                  style={{
                    fontStyle: "normal",
                    fontWeight: "bold",
                    fontSize: "12px",
                    lineHeight: "16px",
                    color: "#808191"
                  }}
                >
                  Volume
                </span>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <span
                    style={{
                      fontWeight: "bold",
                      fontSize: "14px",
                      color: "#FFF"
                    }}
                  >
                    8,645,464.47 USDT
                  </span>
                </div>
              </Col>
            </Row>
          </FloatingNavBar>
        ) : (
          <Row
            style={{
              height: "120px",
              backgroundColor: "#242731",
              borderRadius: 10,
              marginBottom: 30,
              flexWrap: "wrap",
              justifyContent: "space-between"
            }}
          >
            <Col
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                marginLeft: "2%",
                borderRight: "solid 1px",
                marginTop: 10,
                marginBottom: 10,
                paddingRight: 10,
                borderColor: " rgba(228, 228, 228, 0.1)",
                minWidth: "15%"
              }}
            >
              <img src={logo} style={{ height: "3.5em" }} />
              <MarketSelector
                markets={markets}
                setHandleDeprecated={setHandleDeprecated}
                placeholder={"Select market"}
                customMarkets={customMarkets}
                onDeleteCustomMarket={onDeleteCustomMarket}
              />
            </Col>
            <Col
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                minWidth: "15%",
                // borderRight: "solid 1px",
                marginTop: 10,
                marginBottom: 10,
                paddingRight: 40,
                borderColor: " rgba(228, 228, 228, 0.1)"
              }}
            >
              <span
                style={{
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: "12px",
                  lineHeight: "16px",
                  color: "#808191"
                }}
              >
                24h Change
              </span>
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <ArrowUpOutlined
                  style={{
                    fontWeight: "bold",
                    fontSize: "14px",
                    color: "#4FBF67"
                  }}
                />
                <span
                  style={{
                    fontWeight: "bold",
                    fontSize: "14px",
                    color: "#4FBF67"
                  }}
                >
                  +8.42%
                </span>
              </div>
            </Col>
            {/* <Col
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              justifyItems: "flex-start",
              minWidth: "15%",
              borderRight: "solid 1px",
              marginTop: 10,
              marginBottom: 10,
              paddingRight: 10,
              borderColor: " rgba(228, 228, 228, 0.1)"
            }}
          >
            <span
              style={{
                fontStyle: "normal",
                fontWeight: "bold",
                fontSize: "12px",
                lineHeight: "16px",
                color: "#808191"
              }}
            >
              Last Price
            </span>
            <div
              style={{
                display: "flex",
                justifyContent: "flex-start",
                alignItems: "flex-start"
              }}
            >
              <span
                style={{
                  fontWeight: "bold",
                  fontSize: "14px",
                  color: "#FFF"
                }}
              >
                4.39 USDT
              </span>
            </div>
          </Col> */}
            {/* <Col
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              minWidth: "15%",
              borderRight: "solid 1px",
              marginTop: 10,
              marginBottom: 10,
              paddingRight: 10,
              borderColor: " rgba(228, 228, 228, 0.1)"
            }}
          >
            <span
              style={{
                fontStyle: "normal",
                fontWeight: "bold",
                fontSize: "12px",
                lineHeight: "16px",
                color: "#808191"
              }}
            >
              24h Low
            </span>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <span
                style={{
                  fontWeight: "bold",
                  fontSize: "14px",
                  color: "#FFF"
                }}
              >
                4.15 USDT
              </span>
            </div>
          </Col>
          <Col
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              minWidth: "15%",
              borderRight: "solid 1px",
              marginTop: 10,
              marginBottom: 10,
              paddingRight: 10,
              borderColor: " rgba(228, 228, 228, 0.1)"
            }}
          >
            <span
              style={{
                fontStyle: "normal",
                fontWeight: "bold",
                fontSize: "12px",
                lineHeight: "16px",
                color: "#808191"
              }}
            >
              24h Heigh
            </span>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <span
                style={{
                  fontWeight: "bold",
                  fontSize: "14px",
                  color: "#FFF"
                }}
              >
                4.55 USDT
              </span>
            </div>
          </Col>
          <Col
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              minWidth: "15%",
              marginTop: 10,
              marginBottom: 10,
              paddingRight: 10
            }}
          >
            <span
              style={{
                fontStyle: "normal",
                fontWeight: "bold",
                fontSize: "12px",
                lineHeight: "16px",
                color: "#808191"
              }}
            >
              Volume
            </span>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <span
                style={{
                  fontWeight: "bold",
                  fontSize: "14px",
                  color: "#FFF"
                }}
              >
                8,645,464.47 USDT
              </span>
            </div>
          </Col> */}
          </Row>
        )}
        {component}
      </Wrapper>
    </>
  );
}
const filterCustomTokens = async (marketName) => {
  const tokens = await new TokenListProvider().resolve();
  const tokenList = tokens.filterByClusterSlug("mainnet-beta").getList();
  let tokenName = marketName.substr(0, marketName.indexOf("/"));
  console.log("tokenName", tokenName);
  console.log(
    "TOken LIST",
    tokenList.find((elm) => elm.symbol == "SRM")
  );
};
function MarketSelector({
  markets,
  placeholder,
  setHandleDeprecated,
  customMarkets,
  onDeleteCustomMarket
}) {
  const { market, setMarketAddress } = useMarket();

  const onSetMarketAddress = (marketAddress) => {
    setHandleDeprecated(false);
    setMarketAddress(marketAddress);
  };

  const extractBase = (a) => a.split("/")[0];
  const extractQuote = (a) => a.split("/")[1];

  const selectedMarket = getMarketInfos(customMarkets)
    .find(
      (proposedMarket) =>
        market?.address && proposedMarket.address.equals(market.address)
    )
    ?.address?.toBase58();

  return (
    <Select
      showSearch
      size={"large"}
      style={{
        width: 200,
        borderColor: "#d9d9d9 !important",
        boxShadow: "none !important"
      }}
      placeholder={placeholder || "Select a market"}
      optionFilterProp="name"
      onSelect={onSetMarketAddress}
      listHeight={400}
      value={selectedMarket}
      dropdownStyle={{ backgroundColor: "rgb(36, 39, 49)" }}
      filterOption={(input, option) =>
        option?.name?.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
    >
      {customMarkets && customMarkets.length > 0 && (
        <OptGroup label="Custom">
          {customMarkets.map(({ address, name }, i) => (
            <Option
              value={address}
              key={nanoid()}
              name={name}
              style={{
                padding: "10px",
                backgroundColor: "rgb(36, 39, 49)"
                // @ts-ignore
                // backgroundColor: i % 2 === 0 ? "rgb(39, 44, 61)" : null
              }}
            >
              <Row>
                <Col flex="auto">{name}</Col>
                {selectedMarket !== address && (
                  <Col>
                    <DeleteOutlined
                      onClick={(e) => {
                        e.stopPropagation();
                        e.nativeEvent.stopImmediatePropagation();
                        onDeleteCustomMarket && onDeleteCustomMarket(address);
                      }}
                    />
                  </Col>
                )}
              </Row>
            </Option>
          ))}
        </OptGroup>
      )}
      <OptGroup label="Markets">
        {markets
          .sort((a, b) =>
            extractQuote(a.name) === "USDT" && extractQuote(b.name) !== "USDT"
              ? -1
              : extractQuote(a.name) !== "USDT" &&
                extractQuote(b.name) === "USDT"
              ? 1
              : 0
          )
          .sort((a, b) =>
            extractBase(a.name) < extractBase(b.name)
              ? -1
              : extractBase(a.name) > extractBase(b.name)
              ? 1
              : 0
          )
          .map(({ address, name, deprecated }, i) => (
            <Option
              value={address.toBase58()}
              key={nanoid()}
              name={name}
              style={{
                padding: "10px",
                backgroundColor: "rgb(36, 39, 49)"
                // @ts-ignore
                // backgroundColor: i % 2 === 0 ? "rgb(39, 44, 61)" : null
              }}
            >
              {name} {deprecated ? " (Deprecated)" : null}
            </Option>
          ))}
      </OptGroup>
    </Select>
  );
}

const DeprecatedMarketsPage = ({ switchToLiveMarkets }) => {
  return (
    <>
      <Row>
        <Col flex="auto">
          <DeprecatedMarketsInstructions
            switchToLiveMarkets={switchToLiveMarkets}
          />
        </Col>
      </Row>
    </>
  );
};

const RenderNormal = ({ onChangeOrderRef, onPrice, onSize, screenWidth }) => {
  const {
    market,
    marketName,
    customMarkets,
    setCustomMarkets,
    setMarketAddress
  } = useMarket();

  return (
    <Row
      style={{
        minHeight: "900px",
        flexWrap: "nowrap"
      }}
    >
      <Col flex="auto" style={{ height: "50vh" }}>
        <Row
          style={{
            height: "100%",
            display: "flex",
            justifyContent: "space-around",
            justifyItems: "flex-start",
            marginBottom: "0.2em"
          }}
        >
          <Col xs={250} sm={15} style={{ height: "100%", display: "flex" }}>
            <TVChartContainer />
          </Col>
          <Col
            xs={24}
            sm={8}
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-evenly",
              justifyItems: "flex-start"
            }}
          >
            <Orderbook smallScreen={false} onPrice={onPrice} onSize={onSize} />
            <h1
              style={{
                fontFamily: "Poppins",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: "18px",
                lineHeight: "24px",
                color: "#FFFFFF",
                textAlign: "center",
                marginTop: "1.5em"
              }}
            >
              Recent Market Trades
            </h1>
            <TradesTable smallScreen={false} />
          </Col>
        </Row>
        <Row
          style={{
            height: "100%",
            display: "flex",
            justifyContent: "space-evenly",
            justifyItems: "flex-start"
          }}
        >
          <Col xs={250} sm={15}>
            <UserInfoTable />
          </Col>
          <Col
            xs={25}
            sm={9}
            style={{
              height: "100%",
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center"
            }}
            className="tradePage"
          ></Col>
        </Row>
      </Col>
      <Col flex={"360px"} style={{ height: "100%" }}>
        <TradeForm setChangeOrderRef={onChangeOrderRef} />
        <h1
          style={{
            fontFamily: "Poppins",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: "18px",
            lineHeight: "24px",
            color: "#FFFFFF",
            marginTop: "2em",
            textAlign: "center"
          }}
        >
          Current Wallet Balance
        </h1>
        <StandaloneBalancesDisplay />
      </Col>
    </Row>
  );
};

const RenderSmall = ({ onChangeOrderRef, onPrice, onSize, screenWidth }) => {
  return (
    <>
      <Row style={{ height: "30vh" }}>
        <TVChartContainer />
      </Row>
      <Row
        style={{
          height: "900px"
        }}
      >
        <Col flex="auto" style={{ height: "100%", display: "flex" }}>
          <Orderbook
            smallScreen={true}
            depth={13}
            onPrice={onPrice}
            onSize={onSize}
          />
        </Col>
        <Col flex="auto" style={{ height: "100%", display: "flex" }}>
          <TradesTable smallScreen={true} />
        </Col>
        <Col
          flex="400px"
          style={{ height: "100%", display: "flex", flexDirection: "column" }}
        >
          <TradeForm setChangeOrderRef={onChangeOrderRef} />
          <StandaloneBalancesDisplay />
        </Col>
      </Row>
      <Row>
        <Col flex="auto">
          <UserInfoTable />
        </Col>
      </Row>
    </>
  );
};

const RenderSmaller = ({ onChangeOrderRef, onPrice, onSize, screenWidth }) => {
  return (
    <>
      <Row style={{ height: "50vh", marginBottom: "1em" }}>
        <TVChartContainer />
      </Row>
      <Row>
        <Col xs={24} sm={12} style={{ height: "100%", display: "flex" }}>
          <TradeForm style={{ flex: 1 }} setChangeOrderRef={onChangeOrderRef} />
        </Col>
        <Col xs={24} sm={12}>
          <StandaloneBalancesDisplay />
        </Col>
      </Row>
      <Row>
        <Col xs={24} sm={12} style={{ height: "100%", display: "flex" }}>
          <Orderbook smallScreen={true} onPrice={onPrice} onSize={onSize} />
        </Col>
      </Row>
      <Row
        style={{
          height: "500px"
        }}
      >
        <Col xs={24} sm={12} style={{ height: "100%", display: "flex" }}>
          <TradesTable smallScreen={true} />
        </Col>
      </Row>
      <Row>
        <Col flex="auto">
          <UserInfoTable />
        </Col>
      </Row>
    </>
  );
};
