import { useRouteMatch, Route, Switch, Redirect } from "react-router-dom";
import TradePage from "./pages/TradePage";
import OpenOrdersPage from "./pages/OpenOrdersPage";
import React from "react";
import BalancesPage from "./pages/BalancesPage";
import ConvertPage from "./pages/ConvertPage";
import BasicLayout from "./components/BasicLayout";
import ListNewMarketPage from "./pages/ListNewMarketPage";
import NewPoolPage from "./pages/pools/NewPoolPage";
import PoolPage from "./pages/pools/PoolPage";
import PoolListPage from "./pages/pools/PoolListPage";
import { getTradePageUrl } from "./utils/markets";

export function Routes() {
  let { path, url } = useRouteMatch();
  return (
    <>
      <BasicLayout>
        <Switch>
          <Route exact path={path}>
            <Redirect to={getTradePageUrl()} />
          </Route>
          <Route exact path={`${path}/market/:marketAddress`}>
            <TradePage />
          </Route>
          <Route exact path={`${path}/orders`} component={OpenOrdersPage} />
          <Route exact path={`${path}/balances`} component={BalancesPage} />
          <Route exact path={`${path}/convert`} component={ConvertPage} />
          <Route
            exact
            path={`${path}/list-new-market`}
            component={ListNewMarketPage}
          />
          <Route exact path="/serum/pools">
            <PoolListPage />
          </Route>
          <Route exact path="/serum/pools/new">
            <NewPoolPage />
          </Route>
          <Route exact path="/serum/pools/:poolAddress">
            <PoolPage />
          </Route>
        </Switch>
      </BasicLayout>
    </>
  );
}
