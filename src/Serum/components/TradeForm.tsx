import { Button, Input, Radio, Slider, Switch, Select } from "antd";
import React, { useEffect, useState } from "react";
import styled from "styled-components";
import {
  useFeeDiscountKeys,
  useLocallyStoredFeeDiscountKey,
  useMarket,
  useMarkPrice,
  useSelectedBaseCurrencyAccount,
  useSelectedBaseCurrencyBalances,
  useSelectedOpenOrdersAccount,
  useSelectedQuoteCurrencyAccount,
  useSelectedQuoteCurrencyBalances
} from "../utils/markets";
import { useWallet } from "../utils/wallet";
import { notify } from "../utils/notifications";
import {
  floorToDecimal,
  getDecimalCount,
  roundToDecimal
} from "../utils/utils";
import { useSendConnection } from "../utils/connection";
import FloatingElement from "./layout/FloatingElement";
import { getUnixTs, placeOrder } from "../utils/send";
import { SwitchChangeEventHandler } from "antd/es/switch";
import { refreshCache } from "../utils/fetch-loop";
import tuple from "immutable-tuple";
import "./tradeform.css";
import StandaloneBalancesDisplay from "./StandaloneBalancesDisplay";
const { Option } = Select;
const SellButton = styled(Button)`
  margin: 20px 0px 0px 0px;
  background: #f23b69;
  border-color: #f23b69;
  border-radius: 12px;
  min-height: 54px;
`;

const BuyButton = styled(Button)`
  margin: 20px 0px 0px 0px;
  background: #3dbaa2;
  border-color: #3dbaa2;
  border-radius: 12px;
  min-height: 54px;
`;

const sliderMarks = {
  0: "0%",
  25: "25%",
  50: "50%",
  75: "75%",
  100: "100%"
};

export default function TradeForm({
  style,
  setChangeOrderRef
}: {
  style?: any;
  setChangeOrderRef?: (
    ref: ({ size, price }: { size?: number; price?: number }) => void
  ) => void;
}) {
  const [side, setSide] = useState<"buy" | "sell">("buy");
  const [display, setDisplay] = useState("buy");
  const { baseCurrency, quoteCurrency, market } = useMarket();
  const baseCurrencyBalances = useSelectedBaseCurrencyBalances();
  const quoteCurrencyBalances = useSelectedQuoteCurrencyBalances();
  const baseCurrencyAccount = useSelectedBaseCurrencyAccount();
  const quoteCurrencyAccount = useSelectedQuoteCurrencyAccount();
  const openOrdersAccount = useSelectedOpenOrdersAccount(true);
  const { wallet, connected } = useWallet();
  const sendConnection = useSendConnection();
  const markPrice = useMarkPrice();
  useFeeDiscountKeys();
  const { storedFeeDiscountKey: feeDiscountKey } =
    useLocallyStoredFeeDiscountKey();

  const [postOnly, setPostOnly] = useState(false);
  const [ioc, setIoc] = useState(false);
  const [baseSize, setBaseSize] = useState<number | undefined>(undefined);
  const [quoteSize, setQuoteSize] = useState<number | undefined>(undefined);
  const [price, setPrice] = useState<number | undefined>(undefined);
  const [submitting, setSubmitting] = useState(false);
  const [sizeFraction, setSizeFraction] = useState(0);

  const availableQuote =
    openOrdersAccount && market
      ? market.quoteSplSizeToNumber(openOrdersAccount.quoteTokenFree)
      : 0;

  let quoteBalance = (quoteCurrencyBalances || 0) + (availableQuote || 0);
  let baseBalance = baseCurrencyBalances || 0;
  let sizeDecimalCount =
    market?.minOrderSize && getDecimalCount(market.minOrderSize);
  let priceDecimalCount = market?.tickSize && getDecimalCount(market.tickSize);

  const publicKey = wallet?.publicKey;

  useEffect(() => {
    setChangeOrderRef && setChangeOrderRef(doChangeOrder);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setChangeOrderRef]);

  useEffect(() => {
    baseSize && price && onSliderChange(sizeFraction);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [side]);

  useEffect(() => {
    updateSizeFraction();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [price, baseSize]);

  useEffect(() => {
    const warmUpCache = async () => {
      try {
        if (!wallet || !publicKey || !market) {
          console.log(`Skipping refreshing accounts`);
          return;
        }
        const startTime = getUnixTs();
        console.log(`Refreshing accounts for ${market.address}`);
        await market?.findOpenOrdersAccountsForOwner(sendConnection, publicKey);
        await market?.findBestFeeDiscountKey(sendConnection, publicKey);
        const endTime = getUnixTs();
        console.log(
          `Finished refreshing accounts for ${market.address} after ${
            endTime - startTime
          }`
        );
      } catch (e) {
        console.log(`Encountered error when refreshing trading accounts: ${e}`);
      }
    };
    warmUpCache();
    const id = setInterval(warmUpCache, 30_000);
    return () => clearInterval(id);
  }, []);

  const onSetBaseSize = (baseSize: number | undefined) => {
    setBaseSize(baseSize);
    if (!baseSize) {
      setQuoteSize(undefined);
      return;
    }
    let usePrice = price || markPrice;
    if (!usePrice) {
      setQuoteSize(undefined);
      return;
    }
    const rawQuoteSize = baseSize * usePrice;
    const quoteSize =
      baseSize && roundToDecimal(rawQuoteSize, sizeDecimalCount);
    setQuoteSize(quoteSize);
  };

  const onSetQuoteSize = (quoteSize: number | undefined) => {
    setQuoteSize(quoteSize);
    if (!quoteSize) {
      setBaseSize(undefined);
      return;
    }
    let usePrice = price || markPrice;
    if (!usePrice) {
      setBaseSize(undefined);
      return;
    }
    const rawBaseSize = quoteSize / usePrice;
    const baseSize = quoteSize && roundToDecimal(rawBaseSize, sizeDecimalCount);
    setBaseSize(baseSize);
  };

  const doChangeOrder = ({
    size,
    price
  }: {
    size?: number;
    price?: number;
  }) => {
    const formattedSize = size && roundToDecimal(size, sizeDecimalCount);
    const formattedPrice = price && roundToDecimal(price, priceDecimalCount);
    formattedSize && onSetBaseSize(formattedSize);
    formattedPrice && setPrice(formattedPrice);
  };

  const updateSizeFraction = () => {
    const rawMaxSize =
      side === "buy" ? quoteBalance / (price || markPrice || 1) : baseBalance;
    const maxSize = floorToDecimal(rawMaxSize, sizeDecimalCount);
    const sizeFraction = Math.min(((baseSize || 0) / maxSize) * 100, 100);
    setSizeFraction(sizeFraction);
  };

  const onSliderChange = (value) => {
    if (!price && markPrice) {
      let formattedMarkPrice: number | string = priceDecimalCount
        ? markPrice.toFixed(priceDecimalCount)
        : markPrice;
      setPrice(
        typeof formattedMarkPrice === "number"
          ? formattedMarkPrice
          : parseFloat(formattedMarkPrice)
      );
    }

    let newSize;
    if (side === "buy") {
      if (price || markPrice) {
        newSize = ((quoteBalance / (price || markPrice || 1)) * value) / 100;
      }
    } else {
      newSize = (baseBalance * value) / 100;
    }

    // round down to minOrderSize increment
    let formatted = floorToDecimal(newSize, sizeDecimalCount);

    onSetBaseSize(formatted);
  };

  const postOnChange: SwitchChangeEventHandler = (checked) => {
    if (checked) {
      setIoc(false);
    }
    setPostOnly(checked);
  };
  const iocOnChange: SwitchChangeEventHandler = (checked) => {
    if (checked) {
      setPostOnly(false);
    }
    setIoc(checked);
  };

  async function onSubmit() {
    if (!price) {
      console.warn("Missing price");
      notify({
        message: "Missing price",
        type: "error"
      });
      return;
    } else if (!baseSize) {
      console.warn("Missing size");
      notify({
        message: "Missing size",
        type: "error"
      });
      return;
    }

    setSubmitting(true);
    try {
      if (!wallet) {
        return null;
      }

      await placeOrder({
        side,
        price,
        size: baseSize,
        orderType: ioc ? "ioc" : postOnly ? "postOnly" : "limit",
        market,
        connection: sendConnection,
        wallet,
        baseCurrencyAccount: baseCurrencyAccount?.pubkey,
        quoteCurrencyAccount: quoteCurrencyAccount?.pubkey,
        feeDiscountPubkey: feeDiscountKey
      });
      refreshCache(tuple("getTokenAccounts", wallet, connected));
      setPrice(undefined);
      onSetBaseSize(undefined);
    } catch (e) {
      console.warn(e);
      notify({
        message: "Error placing order",
        description: e.message,
        type: "error"
      });
    } finally {
      setSubmitting(false);
    }
  }

  return (
    <FloatingElement
      style={{
        display: "flex",
        flexDirection: "column",
        ...style,
        backgroundColor: "#242731",
        borderRadius: "15px",
        height: "495px"
        // marginBottom: "30px"
      }}
    >
      <div style={{ flex: 1 }}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            marginBottom: 10,
            padding: 10
          }}
        >
          <Button
            onClick={() => {
              setSide("buy");
            }}
            style={{
              background: side == "buy" ? "#3DBAA2" : "#242731",
              borderRadius: "12px",
              borderColor: side == "buy" ? "#191B20" : "#FFF",
              width: "140px",
              height: "48px",
              border: "solid 1px #FFF !important"
            }}
            className={`${side !== "buy" ? "active-sell" : ""}`}
          >
            {" "}
            <span
              style={{
                color: "#FFF",
                textAlign: "center",
                fontWeight: "bold"
              }}
            >
              Buy {baseCurrency}
            </span>
          </Button>
          <Button
            onClick={() => {
              setSide("sell");
            }}
            style={{
              background: side == "sell" ? "#f23b69" : "#242731",
              borderRadius: "12px",
              borderColor: side == "sell" ? "#191B20" : "#FFF !important",
              width: "140px",
              height: "48px"
            }}
            className={`${side !== "sell" ? "active-sell" : ""}`}
          >
            {" "}
            <span
              style={{
                color: "#FFF",
                textAlign: "center",
                fontWeight: "bold"
              }}
            >
              Sell {baseCurrency}
            </span>
          </Button>
        </div>
        {/* */}
        {display != "balance" ? (
          <div id="buy/sell">
            <div style={{ flex: 1 }}>
              <div
                style={{
                  textAlign: "left",
                  backgroundColor: "#191B20",
                  borderRadius: 12,
                  marginBottom: 8
                }}
              >
                <span
                  style={{
                    fontFamily: "Inter",
                    fontStyle: "normal",
                    fontWeight: 600,
                    fontSize: "10px",
                    letterSpacing: "1px",
                    textTransform: "uppercase",
                    color: "#808191",
                    marginLeft: 10
                  }}
                >
                  PRICE
                </span>
                <Input
                  suffix={
                    <span
                      style={{
                        fontFamily: "Inter",
                        fontStyle: "normal",
                        fontWeight: "bold",
                        fontSize: "14px",

                        /* identical to box height, or 171% */
                        textAlign: "right",

                        /* Meaning/Success */
                        color: "#4FBF67"
                      }}
                    >
                      {quoteCurrency}
                    </span>
                  }
                  value={price}
                  type="number"
                  step={market?.tickSize || 1}
                  onChange={(e) => setPrice(parseFloat(e.target.value))}
                />
              </div>
              <div
                style={{
                  textAlign: "left",
                  backgroundColor: "#191B20",
                  borderRadius: 12,
                  marginBottom: 8
                }}
              >
                <span
                  style={{
                    fontFamily: "Inter",
                    fontStyle: "normal",
                    fontWeight: 600,
                    fontSize: "10px",
                    letterSpacing: "1px",
                    textTransform: "uppercase",
                    color: "#808191",
                    marginLeft: 10
                  }}
                >
                  AMOUNT
                </span>
                <Input
                  type="number"
                  suffix={
                    <span
                      style={{
                        fontFamily: "Inter",
                        fontStyle: "normal",
                        fontWeight: "bold",
                        fontSize: "14px",

                        /* identical to box height, or 171% */
                        textAlign: "right",

                        /* Meaning/Success */
                        color: "#FF9F38"
                      }}
                    >
                      {baseCurrency}
                    </span>
                  }
                  value={baseSize}
                  step={market?.minOrderSize || 1}
                  onChange={(e) => onSetBaseSize(parseFloat(e.target.value))}
                />
              </div>
              <div
                style={{
                  textAlign: "left",
                  backgroundColor: "#191B20",
                  borderRadius: 12,
                  marginBottom: 8
                }}
              >
                <span
                  style={{
                    fontFamily: "Inter",
                    fontStyle: "normal",
                    fontWeight: 600,
                    fontSize: "10px",
                    letterSpacing: "1px",
                    textTransform: "uppercase",
                    color: "#808191",
                    marginLeft: 10
                  }}
                >
                  TOTAL
                </span>
                <Input
                  style={{ padding: "-2em" }}
                  type="number"
                  suffix={
                    <span
                      style={{
                        fontFamily: "Inter",
                        fontStyle: "normal",
                        fontWeight: "bold",
                        fontSize: "14px",
                        /* identical to box height, or 171% */
                        textAlign: "right",
                        /* Meaning/Success */
                        color: "#4FBF67"
                      }}
                    >
                      {quoteCurrency}
                    </span>
                  }
                  value={quoteSize}
                  step={market?.minOrderSize || 1}
                  onChange={(e) => onSetQuoteSize(parseFloat(e.target.value))}
                />
              </div>

              <Slider
                value={sizeFraction}
                tipFormatter={(value) => `${value}%`}
                marks={sliderMarks}
                onChange={onSliderChange}
              />
              <div style={{ paddingTop: 18 }}>
                {"POST "}
                <Switch
                  checked={postOnly}
                  onChange={postOnChange}
                  style={{ marginRight: 40 }}
                />
                {"IOC "}
                <Switch checked={ioc} onChange={iocOnChange} />
              </div>
            </div>
            {side === "buy" ? (
              <BuyButton
                disabled={!price || !baseSize}
                onClick={onSubmit}
                block
                type="primary"
                size="large"
                loading={submitting}
              >
                Buy {baseCurrency}
              </BuyButton>
            ) : (
              <SellButton
                disabled={!price || !baseSize}
                onClick={onSubmit}
                block
                type="primary"
                size="large"
                loading={submitting}
              >
                Sell {baseCurrency}
              </SellButton>
            )}
          </div>
        ) : (
          <StandaloneBalancesDisplay />
        )}
      </div>
    </FloatingElement>
  );
}
