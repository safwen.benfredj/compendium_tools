import { Col, Row } from "antd";
import React, { useRef, useEffect, useState } from "react";
import styled, { css } from "styled-components";
import { useMarket, useOrderbook, useMarkPrice } from "../utils/markets";
import { isEqual, getDecimalCount } from "../utils/utils";
import { useInterval } from "../utils/useInterval";
import FloatingElement from "./layout/FloatingElement";
import usePrevious from "../utils/usePrevious";
import { ArrowUpOutlined, ArrowDownOutlined } from "@ant-design/icons";
import FloatingElementBlock from "./layout/FloatingElementBlock";
import { ConsumeEventsDetailsCard } from "../../explorer/components/instruction/mango/ConsumeEventsDetailsCard";

const Title = styled.div`
  color: rgba(255, 255, 255, 1);
  margin-bottom: 1.2em;
`;

const SizeTitle = styled(Row)`
  /* padding: 0px 0 14px; */
  color: #434a59;
`;

const MarkPriceTitle = styled(Row)`
  padding: 8px 0 14px;
  font-weight: 700;
  border-top: 1px solid #434a59;
  border-bottom: 1px solid #434a59;
`;

const Line = styled.div`
  text-align: left;
  float: left;
  height: 100%;
  ${(props) =>
    props["data-width"] &&
    css`
      width: ${props["data-width"]};
    `}
  ${(props) =>
    props["data-bgcolor"] &&
    css`
      background-color: ${props["data-bgcolor"]};
    `}
`;

const Price = styled.div`
  position: absolute;
  right: 5px;
  color: white;
  top: 5px;
`;

export default function Orderbook({ smallScreen, depth = 7, onPrice, onSize }) {
  const markPrice = useMarkPrice();
  const [orderbook] = useOrderbook();
  const { baseCurrency, quoteCurrency } = useMarket();

  const currentOrderbookData = useRef(null);
  const lastOrderbookData = useRef(null);

  const [orderbookData, setOrderbookData] = useState(null);

  useInterval(() => {
    if (
      !currentOrderbookData.current ||
      JSON.stringify(currentOrderbookData.current) !==
        JSON.stringify(lastOrderbookData.current)
    ) {
      let bids = orderbook?.bids || [];
      let asks = orderbook?.asks || [];

      let sum = (total, [, size], index) =>
        index < depth ? total + size : total;
      let totalSize = bids.reduce(sum, 0) + asks.reduce(sum, 0);

      let bidsToDisplay = getCumulativeOrderbookSide(bids, totalSize, false);
      let asksToDisplay = getCumulativeOrderbookSide(asks, totalSize, true);

      currentOrderbookData.current = {
        bids: orderbook?.bids,
        asks: orderbook?.asks
      };

      setOrderbookData({ bids: bidsToDisplay, asks: asksToDisplay });
    }
  }, 250);

  useEffect(() => {
    lastOrderbookData.current = {
      bids: orderbook?.bids,
      asks: orderbook?.asks
    };
  }, [orderbook]);

  function getCumulativeOrderbookSide(orders, totalSize, backwards = false) {
    let cumulative = orders
      .slice(0, depth)
      .reduce((cumulative, [price, size], i) => {
        const cumulativeSize = (cumulative[i - 1]?.cumulativeSize || 0) + size;
        cumulative.push({
          price,
          size,
          cumulativeSize,
          sizePercent: Math.round((cumulativeSize / (totalSize || 1)) * 100)
        });
        return cumulative;
      }, []);
    if (backwards) {
      cumulative = cumulative.reverse();
    }
    return cumulative;
  }

  return (
    <FloatingElementBlock
      className="order_book"
      style={
        smallScreen
          ? {
              flex: 1,
              margin: "5px",
              minWidth: "368px",
              padding: "0 !important",
              backgroundColor: "rgb(36, 39, 49)",
              borderRadius:"15px"
            }
          : {
              overflow: "hidden",

              boxSizing: "border-box",
              minWidth: "368px",
              backgroundColor: "rgb(36, 39, 49)",
              width: "100%",
              height: "495px",
              borderRadius: "15px",
              padding: "0 !important"
            }
      }
    >
      <SizeTitle
        style={{
          display: "flex",
          justifyContent: "space-evenly",
          alignItems: "center"
        }}
        className="remove-padding"
      >
        <Col
          style={{
            fontFamily: "Inter",
            fontWeight: 600,
            fontSize: "10px",
            lineHeight: "24px",

            letterSpacing: "1px",
            textTransform: "uppercase",
            color: "#808191"
          }}
        >
          PRICE ({quoteCurrency})
        </Col>
        <Col
          style={{
            fontFamily: "Inter",
            fontWeight: 600,
            fontSize: "10px",
            lineHeight: "24px",

            letterSpacing: "1px",
            textTransform: "uppercase",
            color: "#808191"
          }}
        >
          AMOUNT ({baseCurrency})
        </Col>
        <Col
          style={{
            fontFamily: "Inter",
            fontWeight: 600,
            fontSize: "10px",
            lineHeight: "24px",

            letterSpacing: "1px",
            textTransform: "uppercase",
            color: "#808191"
          }}
        >
          TOTAL ({quoteCurrency})
        </Col>
      </SizeTitle>
      <div style={{ width: "100%", height: "100%", overflowY: "auto" }}>
        {orderbookData?.asks.map(({ price, size, sizePercent }) => (
          <OrderbookRow
            key={price + ""}
            price={price}
            size={size}
            side={"sell"}
            sizePercent={sizePercent}
            onPriceClick={() => onPrice(price)}
            onSizeClick={() => onSize(size)}
          />
        ))}
        <MarkPriceComponent markPrice={markPrice} baseCurrency={quoteCurrency} />
        {orderbookData?.bids.map(({ price, size, sizePercent }) => (
          <OrderbookRow
            key={price + ""}
            price={price}
            size={size}
            side={"buy"}
            sizePercent={sizePercent}
            onPriceClick={() => onPrice(price)}
            onSizeClick={() => onSize(size)}
          />
        ))}
      </div>
    </FloatingElementBlock>
  );
}

const OrderbookRow = React.memo(
  ({ side, price, size, sizePercent, onSizeClick, onPriceClick }) => {
    const element = useRef();

    const { market } = useMarket();

    useEffect(() => {
      // eslint-disable-next-line
      !element.current?.classList.contains("flash") &&
        element.current?.classList.add("flash");
      const id = setTimeout(
        () =>
          element.current?.classList.contains("flash") &&
          element.current?.classList.remove("flash"),
        250
      );
      return () => clearTimeout(id);
    }, [price, size]);

    let formattedSize =
      market?.minOrderSize && !isNaN(size)
        ? Number(size).toFixed(getDecimalCount(market.minOrderSize) + 1)
        : size;

    let formattedPrice =
      market?.tickSize && !isNaN(price)
        ? Number(price).toFixed(getDecimalCount(market.tickSize) + 1)
        : price;

    return (
      <Row
        ref={element}
        style={{
          marginBottom: 1,
          paddingLeft: "0.2em",
          paddingRight: "0.2em",
          height: "31px"
        }}
        onClick={onSizeClick}
      >
        <Col
          span={8}
          style={{
            textAlign: "left",
            alignItems: "flex-start"
          }}
        >
          <Price
            onClick={onPriceClick}
            style={{
              color:
                side === "buy"
                  ? "rgba(61, 186, 162, 1)"
                  : "rgba(255, 122, 104, 1)",
              left: 20,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontWeight: 500,
              fontSize: "12px",
              lineHeight: "16px"
            }}
          >
            {formattedPrice}
          </Price>
          <Line
            data-width={sizePercent + "%"}
            data-bgcolor={
              side === "buy"
                ? "rgba(61, 186, 162, 0.4)"
                : "rgba(255, 122, 104, 0.4)"
            }
          />
        </Col>
        <Col
          span={8}
          style={{
            textAlign: "center",
            fontFamily: "Inter",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: "12px",
            lineHeight: "16px",
            marginTop: "5px"
          }}
        >
          {formattedSize}
        </Col>
        <Col
          span={8}
          style={{
            textAlign: "center",
            fontFamily: "Inter",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: "12px",
            lineHeight: "16px",
            marginTop: "5px"
          }}
        >
          {(Number(formattedSize) * Number(formattedPrice)).toFixed(2)}
        </Col>
      </Row>
    );
  },
  (prevProps, nextProps) =>
    isEqual(prevProps, nextProps, ["price", "size", "sizePercent"])
);

const MarkPriceComponent = React.memo(
  ({ markPrice, baseCurrency }) => {
    const { market } = useMarket();
    const previousMarkPrice = usePrevious(markPrice);

    let markPriceColor =
      markPrice > previousMarkPrice
        ? "#41C77A"
        : markPrice < previousMarkPrice
        ? "#F23B69"
        : "white";

    let formattedMarkPrice =
      markPrice &&
      market?.tickSize &&
      markPrice.toFixed(getDecimalCount(market.tickSize));

    return (
      <MarkPriceTitle justify="center">
        <Col style={{ color: markPriceColor, fontSize: "16px" }}>
          {markPrice > previousMarkPrice && (
            <ArrowUpOutlined style={{ marginRight: 5 }} />
          )}
          {markPrice < previousMarkPrice && (
            <ArrowDownOutlined style={{ marginRight: 5 }} />
          )}
          {formattedMarkPrice || "----"} {baseCurrency}
        </Col>
      </MarkPriceTitle>
    );
  },
  (prevProps, nextProps) => isEqual(prevProps, nextProps, ["markPrice"])
);
