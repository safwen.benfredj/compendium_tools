import BalancesTable from "./BalancesTable";
import OpenOrderTable from "./OpenOrderTable";
import React from "react";
import { Tabs, Typography } from "antd";
import FillsTable from "./FillsTable";
import FloatingElement from "../layout/FloatingElement";
import FeesTable from "./FeesTable";
import { useOpenOrders, useBalances, useMarket } from "../../utils/markets";
import "./styles.css";
const { Paragraph } = Typography;
const { TabPane } = Tabs;

export default function Index() {
  const { market } = useMarket();
  return (
    <FloatingElement
      style={{
        flex: 1,
        backgroundColor: "rgb(36, 39, 49)",
        borderRadius: "16px",
        paddingTop: 20,
        backgroundColor: "transparent",
      }}
    >
      <Tabs
        defaultActiveKey="orders"
        tabBarStyle={{
          backgroundColor: "transparent",
          marginBottom: "20px"
        }}
      >
        <TabPane tab="Open Orders" key="orders">
          <OpenOrdersTab />
        </TabPane>
        <TabPane tab="Recent Trade History" key="fills">
          <FillsTable />
        </TabPane>
        <TabPane tab="Balances" key="balances">
          <BalancesTab />
        </TabPane>
        {market && market.supportsSrmFeeDiscounts ? (
          <TabPane tab="Fee discounts" key="fees">
            <FeesTable />
          </TabPane>
        ) : null}
      </Tabs>
    </FloatingElement>
  );
}

const OpenOrdersTab = () => {
  const openOrders = useOpenOrders();

  return <OpenOrderTable openOrders={openOrders} />;
};

const BalancesTab = () => {
  const balances = useBalances();

  return <BalancesTable balances={balances} />;
};
