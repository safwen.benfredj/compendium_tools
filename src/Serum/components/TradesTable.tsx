import { Col, Row } from "antd";
import React from "react";
import styled from "styled-components";
import { useMarket, useBonfidaTrades } from "../utils/markets";
import { getDecimalCount } from "../utils/utils";
import FloatingElement from "./layout/FloatingElement";
import { BonfidaTrade } from "../utils/types";

const Title = styled.div`
  color: rgba(255, 255, 255, 1);
`;
const SizeTitle = styled(Row)`
  padding: 0px 0 14px;
  color: #434a59;
`;

export default function PublicTrades({ smallScreen }) {
  const { baseCurrency, quoteCurrency, market } = useMarket();
  const [trades, loaded] = useBonfidaTrades();

  return (
    <FloatingElement
      style={
        smallScreen
          ? {
              flex: 1,

              background: "#242731",
              margin: "5px",
              minWidth: "368px",
              padding: "0 !important",
              marginTop:"20px",
              borderRadius: "15px",
            }
          : {
              display: "flex",
              flexDirection: "column",
              backgroundColor: "#242731",
              borderRadius: "15px",
              minHeight: "495px",
              minWidth: "405px",
              paddingTop: "0px !important",
              marginBottom:"2em"
            }
      }
    >
      <SizeTitle
        style={{
          borderBottom: "1px solid #434a59",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center"
        }}
      >
        <Col
          style={{
            fontFamily: "Inter",
            fontWeight: 600,
            fontSize: "10px",
            lineHeight: "24px",

            letterSpacing: "1px",
            textTransform: "uppercase",
            color: "#808191"
          }}
        >
          TIME
        </Col>
        <Col
          style={{
            fontFamily: "Inter",
            fontWeight: 600,
            fontSize: "10px",
            lineHeight: "24px",

            letterSpacing: "1px",
            textTransform: "uppercase",
            color: "#808191"
          }}
        >
          PRICE ({quoteCurrency}){" "}
        </Col>
        <Col
          style={{
            fontFamily: "Inter",
            fontWeight: 600,
            fontSize: "10px",
            lineHeight: "24px",

            letterSpacing: "1px",
            textTransform: "uppercase",
            color: "#808191"
          }}
        >
          AMOUNT ({baseCurrency})
        </Col>
        <Col
          style={{
            fontFamily: "Inter",
            fontWeight: 600,
            fontSize: "10px",
            lineHeight: "24px",

            letterSpacing: "1px",
            textTransform: "uppercase",
            color: "#808191"
          }}
        >
          TOTAL ({quoteCurrency})
        </Col>
      </SizeTitle>
      {!!trades && loaded && (
        <div
          style={{
            marginRight: "-20px",
            paddingRight: "5px",
            overflowY: "scroll",
            maxHeight: smallScreen ? "calc(100% - 75px)" : "calc(100vh - 500px)"
          }}
        >
          {trades.map((trade: BonfidaTrade, i: number) => (
            <Row
              key={i}
              style={{ marginBottom: 4, borderBottom: "1px solid #434a59" }}
            >
              <Col
                span={6}
                style={{
                  textAlign: "left",
                  color: "#FFF",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: 500,
                  fontSize: "12px",
                  lineHeight: "16px"
                }}
              >
                {trade.time && new Date(trade.time).toLocaleTimeString()}
              </Col>
              <Col
                span={6}
                style={{
                  color:
                    trade.side === "buy"
                      ? "rgba(61, 186, 162, 1)"
                      : "rgba(255, 122, 104, 1)",
                  textAlign: "center",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: 500,
                  fontSize: "12px",
                  lineHeight: "16px"
                }}
              >
                {market?.tickSize && !isNaN(trade.price)
                  ? Number(trade.price).toFixed(
                      getDecimalCount(market.tickSize)
                    )
                  : trade.price}
              </Col>
              <Col
                span={6}
                style={{
                  textAlign: "center",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: 500,
                  fontSize: "12px",
                  lineHeight: "16px"
                }}
              >
                {market?.minOrderSize && !isNaN(trade.size)
                  ? Number(trade.size).toFixed(
                      getDecimalCount(market.minOrderSize)
                    )
                  : trade.size}
              </Col>
              <Col
                span={6}
                style={{
                  textAlign: "center",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: 500,
                  fontSize: "12px",
                  lineHeight: "16px"
                }}
              >
                {market?.minOrderSize && !isNaN(trade.size)
                  ? Number(trade.size).toFixed(
                      getDecimalCount(market.minOrderSize)
                    )
                  : trade.size}
              </Col>
            </Row>
          ))}
        </div>
      )}
    </FloatingElement>
  );
}
