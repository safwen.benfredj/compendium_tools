export const OPEN_SIDEBAR = "OPEN_SIDEBAR";
export const CLOSE_SIDEBAR = "CLOSE_SIDEBAR";

export const OPEN_HOME = "OPEN_HOME_PAGE";
export const OPEN_SERUM = "OPEN_SERUM";
export const OPEN_EXPLORER = "OPEN_EXPLORER";