import React, { useReducer } from 'react';
import { CLOSE_SIDEBAR, OPEN_SIDEBAR } from '../actionTypes';
import sidebarReducer from '../reducers/sidebarreducer';
import SideBarContext from '../context/sidebarcontext';
const SideBarState = props => {
    const initialState = {
        open: false
    };

    const [state, dispatch] = useReducer(sidebarReducer, initialState);

    const openSidebar = () => {
        dispatch({
            type: OPEN_SIDEBAR
        })
    }
    const closeSidebar = () => {
        console.log("SideBar Context")
        dispatch({
            type: CLOSE_SIDEBAR
        })

    }

    return (
        <SideBarContext.Provider
            value={{
                open: state.open,
                closeSidebar,
                openSidebar
            }}
        >
            {props.children}
        </SideBarContext.Provider>
    );
}
export default SideBarState