import React, { useReducer } from 'react';
import { CLOSE_SIDEBAR, OPEN_EXPLORER, OPEN_HOME, OPEN_SERUM, OPEN_SIDEBAR } from '../actionTypes';
import NavContext from '../context/NavContext';
import NavReducer from '../reducers/navReducer';
const NavProvider = props => {
    const initialState = {
        selectedUrl: '/home'
    };

    const [state, dispatch] = useReducer(NavReducer, initialState);

    const homePage = () => {
        dispatch({
            type: OPEN_HOME
        })
    }
    const tradePage = () => {
        console.log("SideBar Context")
        dispatch({
            type: OPEN_SERUM
        })

    }
    const explorerPage = () => {
        dispatch({
            type: OPEN_EXPLORER
        })
    }

    return (
        <NavContext.Provider
            value={{
                selectedUrl: state.selectedUrl,
                homePage,
                tradePage,
                explorerPage

            }}
        >
            {props.children}
        </NavContext.Provider>
    );
}
export default NavProvider