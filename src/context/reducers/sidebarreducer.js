import { OPEN_SIDEBAR, CLOSE_SIDEBAR } from '../actionTypes';
const sidebarReducer = (state, action) => {
    let { type, payload } = action;

    switch (type) {
        case OPEN_SIDEBAR:
            return { ...state, open: true }
        case CLOSE_SIDEBAR:
            return { ...state, open: false }

        default:
            return state;
    }

}

export default sidebarReducer;