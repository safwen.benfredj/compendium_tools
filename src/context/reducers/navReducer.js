import { OPEN_EXPLORER, OPEN_HOME, OPEN_SERUM } from "../actionTypes";

const NavReducer = (state, action) => {
    let { type } = action;
    switch (type) {
        case OPEN_EXPLORER:
            return { ...state, selectedUrl: '/explorer' }
        case OPEN_SERUM:
            return { ...state, selectedUrl: '/markets' }
        case OPEN_HOME:
            return { ...state, selectedUrl: '/home' }

        default:
            return state;
    }

}

export default NavReducer ; 