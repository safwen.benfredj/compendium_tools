/* eslint-disable @typescript-eslint/no-redeclare */
import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { ClusterModal } from "./components/ClusterModal";
import { MessageBanner } from "./components/MessageBanner";
import { Navbar } from "./components/Navbar";
import { ClusterStatusBanner } from "./components/ClusterStatusButton";
import { SearchBar } from "./components/SearchBar";

import { AccountDetailsPage } from "./pages/AccountDetailsPage";
import { TransactionInspectorPage } from "./pages/inspector/InspectorPage";
import { ClusterStatsPage } from "./pages/ClusterStatsPage";
import { SupplyPage } from "./pages/SupplyPage";
import { TransactionDetailsPage } from "./pages/TransactionDetailsPage";
import { BlockDetailsPage } from "./pages/BlockDetailsPage";

const ADDRESS_ALIASES = ["account", "accounts", "addresses"];
const TX_ALIASES = ["txs", "txn", "txns", "transaction", "transactions"];

function App() {
  return (
    <>
      <ClusterModal />
      <div className="main-content pb-4">
        <Navbar />
        <MessageBanner />
        <ClusterStatusBanner />
        <SearchBar />
        <Switch>
          <Route
            exact
            path={[
              "/explorer/supply",
              "/explorer/accounts",
              "/explorer/accounts/top"
            ]}
          >
            <SupplyPage />
          </Route>
          <Route
            exact
            path={TX_ALIASES.map((tx) => `/${tx}/:signature`)}
            render={({ match, location }) => {
              let pathname = `/tx/${match.params.signature}`;
              return <Redirect to={{ ...location, pathname }} />;
            }}
          />
          <Route
            exact
            path={["/explorer/tx/inspector", "/tx/:signature/inspect"]}
            render={({ match }) => {
              // @ts-ignore
              return (
                <TransactionInspectorPage signature={match.params.signature} />
              );
            }}
          />
          <Route
            exact
            path={"/explorer/tx/:signature"}
            render={({ match }) => (
              <TransactionDetailsPage signature={match.params.signature} />
            )}
          />
          <Route
            exact
            path={["/explorer/block/:id", "/explorer/block/:id/:tab"]}
            render={({ match }) => (
              <BlockDetailsPage slot={match.params.id} tab={match.params.tab} />
            )}
          />
          <Route
            exact
            path={[
              ...ADDRESS_ALIASES.map((path) => `/explorer/${path}/:address`),
              ...ADDRESS_ALIASES.map(
                (path) => `/explorer/${path}/:address/:tab`
              )
            ]}
            render={({ match, location }) => {
              let pathname = `/explorer/address/${match.params.address}`;
              if (match.params.tab) {
                pathname += `/${match.params.tab}`;
              }
              return <Redirect to={{ ...location, pathname }} />;
            }}
          />
          <Route
            exact
            path={[
              "/explorer/address/:address",
              "/explorer/address/:address/:tab"
            ]}
            render={({ match }) => (
              <AccountDetailsPage
                address={match.params.address}
                tab={match.params.tab}
              />
            )}
          />
          <Route exact path="/explorer">
            <ClusterStatsPage />
          </Route>
          <Route
            render={({ location }) => (
              <Redirect to={{ ...location, pathname: "/explorer" }} />
            )}
          />
        </Switch>
      </div>
    </>
  );
}

export default App;
