import React, { useContext, useState } from "react";
import homeIcon from "../assets/img/home.png";
import spotMarket from "../assets/img/spot-market.png";
import futureIcon from "../assets/img/future.png";
import poolsIcon from "../assets/img/pools.png";
import signalProvider from "../assets/img/signal.png";
import strategy from "../assets/img/strategy.png";
import tokenIcon from "../assets/img/token.png";
import walletIcon from "../assets/img/wallet.png";
import "./styles.css";
import mainLogo from "../assets/img/comp-logo.svg";
import SideBarContext from "../context/context/sidebarcontext";
import NavContext from "../context/context/NavContext";
import { Link, useLocation } from "react-router-dom";
import { v4 as uuidv4 } from 'uuid';
const Sidebar = () => {
  const [displaySideBar, setSideBar] = useState(false);
  const { open, openSidebar, closeSidebar } = useContext(SideBarContext);
  const { selectedUrl, homePage, tradePage, explorerPage } = useContext(NavContext);
  const location = useLocation();
  const toggleSideBar = () => {
    if (open) {
      closeSidebar();
    } else {
      openSidebar();
    }
  };
  const linkTarget = {
    pathname: "/",
    key: uuidv4(), // we could use Math.random, but that's not guaranteed unique.
    state: {
      applied: true
    }
  };
  return (
    <div className={`sidebar ${open ? "active" : ""} `}>
      <div className="sidebar__head">
        <a className="sidebar__logo" href="index.html">
          <img className="sidebar__pic sidebar__pic_light" src={mainLogo} />
          <img className="sidebar__pic sidebar__pic_dark" src={mainLogo} />
        </a>
        <button className="sidebar__toggle" onClick={toggleSideBar}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width={24}
            height={24}
            fill="none"
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
            viewBox="0 0 24 24"
          >
            <path d="M22 12H3" stroke="#11142d" />
            <g stroke="#808191">
              <path d="M22 4H13" />
              <path opacity=".301" d="M22 20H13" />
            </g>
            <path d="M7 7l-5 5 5 5" stroke="#11142d" />
          </svg>
        </button>
        <button
          className="sidebar__close"
          onClick={() => {
            closeSidebar();
          }}
        >
          <svg className="icon icon-close">
            <use xlinkHref="img/sprite.svg#icon-close" />
          </svg>
        </button>
      </div>
      <div className="sidebar__body">
        <nav className="sidebar__nav">
          <Link className={`sidebar__item ${location.pathname == '/' ? 'active' : ''}`} to={linkTarget}>
            <div className="sidebar__icon">
              {" "}<img src={homeIcon} />
            </div>
            <div className="sidebar__text">Dashboard</div>
          </Link>
          <Link className={`sidebar__item ${location.pathname.includes('/serum') ? 'active' : ''}`} to="/serum">
            <div className="sidebar__icon">
              <img src={spotMarket} />
            </div>
            <div className="sidebar__text">Spot Markets</div>
          </Link>
          <a className="sidebar__item" href="prices.html">
            <div className="sidebar__icon">
              <img src={futureIcon} />
            </div>
            <div className="sidebar__text">Simple Swap</div>
          </a>
          <a className="sidebar__item" href="wallets.html">
            <div className="sidebar__icon">
              <img src={poolsIcon} />
            </div>
            <div className="sidebar__text">Trading Pools</div>
          </a>
          <a className="sidebar__item" href="promotions.html">
            <div className="sidebar__icon">
              <img src={signalProvider} />
            </div>
            <div className="sidebar__text">Metaverse</div>
          </a>
          <Link className="sidebar__item" className={`sidebar__item ${location.pathname == '/explorer' ? 'active' : ''}`} to="/explorer">
            <div className="sidebar__icon">
              <img src={signalProvider} />
            </div>
            <div className="sidebar__text">Network Explorer</div>
          </Link>
          <a
            className="sidebar__item js-popup-open"
            href="#popup-settings"
            data-effect="mfp-zoom-in"
          >
            <div className="sidebar__icon">
              <img src={walletIcon} />
            </div>
            <div className="sidebar__text">Watch Wallet</div>
          </a>
        </nav>
        <div className="sidebar__mail">
          <button className="sidebar__close">
            <svg className="icon icon-close">
              <use xlinkHref="img/sprite.svg#icon-close" />
            </svg>
          </button>
          <div className="sidebar__info">Automate your FTX porfolio</div>
          <a className="sidebar__btn btn btn_white btn_sm" href="https://compendium.finance/cextools" target="_blank">
            Click Here
          </a>
        </div>
        <form className="sidebar__search">
          <input className="sidebar__input" type="text" placeholder="Search" />
          <button className="sidebar__start">
            <svg className="icon icon-search">
              <use xlinkHref="img/sprite.svg#icon-search" />
            </svg>
          </button>
        </form>
      </div>
      {/* <div className="sidebar__bottom">
        <label className="switch switch_theme js-switch-theme">
          <input className="switch__input" type="checkbox" />
          <span className="switch__in">
            <span className="switch__box" />
            <span className="switch__icon">
              <svg className="icon icon-theme-light">
                <use xlinkHref="img/sprite.svg#icon-theme-light" />
              </svg>
              <svg className="icon icon-theme-dark">
                <use xlinkHref="img/sprite.svg#icon-theme-dark" />
              </svg>
            </span>
          </span>
        </label>
        <a className="sidebar__user" href="sign-in.html">
          <img src="img/ava-header.png" alt />
        </a>
      </div> */}
    </div>
  );
};

export default Sidebar;
