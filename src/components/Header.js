import React, { useState } from 'react'
import WalletConnect from '../Serum/components/WalletConnect';
import mainLogo from '../assets/img/comp-logo.svg'
const Header = () => {
    const [displayMenu, setDisplayMenu] = useState(false);
    return (
        <div className="header">
            <form className="header__search"><input className="header__input" type="text" placeholder="Search" /><button className="header__start"><svg className="icon icon-search">
                <use xlinkHref="img/sprite.svg#icon-search" />
            </svg></button></form><a className="header__logo" href="index.html"><img src={mainLogo} /></a>
            <div className="header__group">

                <div className={`header__item header__item_lang ${displayMenu ? "active" : ""}`}><button className="header__head" onClick={() => {
                    setDisplayMenu(!displayMenu);
                }}>Eng/USD</button>
                    <div className="header__body">
                        <div className="header__lang ">
                            <div className="header__cell"><a className="header__link active" href="#"><span className="header__flag">🇺🇸</span> English</a><a className="header__link" href="#"><span className="header__flag">🇨🇳</span> 中文</a><a className="header__link" href="#"><span className="header__flag">🇪🇸</span> Española</a><a className="header__link" href="#"><span className="header__flag">🇫🇷</span> Français</a><a className="header__link" href="#"><span className="header__flag">🇻🇳</span> Tiếng Việt</a></div>
                            <div className="header__cell"><a className="header__link active" href="#">USD</a><a className="header__link" href="#">EUR</a><a className="header__link" href="#">JPY</a><a className="header__link" href="#">BTC</a></div>
                        </div>
                    </div>
                </div>
            </div><a className="header__user" >
                {/* <button className="widgets__btn btn btn_blue">Connect Wallet</button> */}
                <WalletConnect />
            </a>
        </div>
    )
}

export default Header
