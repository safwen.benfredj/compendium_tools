import React, { useContext } from 'react'
import SideBarContext from '../context/context/sidebarcontext'
import Header from '../components/Header';
import Serum from '../Serum/App';
import Explorer from '../explorer';
import { Route, Switch } from 'react-router-dom';
import Home from './Home';
import NavContext from '../context/context/NavContext';
import NotFound from './NotFound';

const Main = () => {
    const { open } = useContext(SideBarContext);

    return (

        <div
            className={`${!open ? "page__content" : "page__content_active"}`}
        >   <Header />
            <Switch>

                <Route path="/serum" component={Serum} />
                <Route path="/explorer" component={Explorer} />

                <Route exact path="/" component={Home} />
            </Switch>
        </div>

    )
}


export default Main
