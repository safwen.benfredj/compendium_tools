FROM node:14-alpine 
# Add a work directory
WORKDIR /app
# Cache and Install dependencies
COPY package.json .

RUN yarn install 
# Copy app files
COPY . .
EXPOSE 3000
# Build the app
CMD [ "yarn","start" ]

